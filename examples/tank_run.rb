#!/usr/bin/env ruby

require 'tank_api_client'
require 'inifile'
require 'influxdb'
require 'slop'

# Options
opts = Slop.parse do |o|
  o.string '-l', '--load', 'load.ini file', default: 'load.ini'
  o.string '-r', '--report', 'report in jenkins'
  o.string '-a', '--api', 'api server host', default: 'localhost'
  o.on '-h', '--help' do
    puts o
    exit
  end
end

begin
  # Start tank
  load_ini = IniFile.load(opts[:load])
  host = opts[:api]
  tank = TankApiClient.new(host)
  id = tank.run load_ini.to_s
  puts "tank run" if tank.start id
  start_time = Time.now.to_i
  sleep 5

  # Console report
  stage = tank.get_stage(id)
  status = tank.get_status(id)
  if status["status"] == "failed"
    raise status["failures"].to_s
  else
    while stage != "finished"
      stage = tank.get_stage(id)
      status = tank.get_status(id)
      if status["status"] == "failed"
        puts status
        tank.stop(id)
        #exit 1
      elsif stage == "poll"
        #print "\r#{status}"
      end
      sleep 1
    end
  end

  # InfluxDB REPORT
  if load_ini.has_section?('loadtest')
    max_rps = tank.get_max_rps(id)
    response_time = tank.get_avg_time_max(id)

    username = 'root'
    password = 'root'
    database = 'mydb'
    name     = 'max_rps'

    influxdb = InfluxDB::Client.new database, 
                                    host:     load_ini['loadtest']['address'], 
                                    port:     load_ini['loadtest']['port'], 
                                    username: load_ini['loadtest']['user'], 
                                    password: load_ini['loadtest']['passwd']

    rand_string = 10.times.map { [*'0'..'9', *'a'..'z'].sample }.join

    report_tags = { branch:  load_ini['loadtest']['branch'],
                    project: load_ini['loadtest']['project'],
                    method:  load_ini['loadtest']['method'],
                    id:      rand_string }

    rps_data = {
      values: { maxrps: max_rps, response_time: response_time },
      tags: report_tags,
      timestamp: Time.now.to_i
    }

    influxdb.write_point(name, rps_data)
  end

  # Console report
  if tank.get_status(id)["status"] == "success"
    codes = tank.get_request_codes(id)
    puts "Load Schema: "                        + "#{load_ini['phantom']['rps_schedule']}"
    puts "Средний RPS: "                        + "#{tank.get_average_rps(id)} rps"
    puts "Максимальный RPS:"                    + "#{tank.get_max_rps(id)} rps"
    puts "Среднее время ответа:"                + "#{tank.get_average_time(id)} ms"
    puts "\Время ответа при максимальном RPS: " + "#{tank.get_avg_time_max(id)} ms"
    puts "Ошибки net: "                         + "#{codes["net"]}" unless codes["net"].empty?
    puts "Ошибки HTTP: "                        + "#{codes["http"]}" unless codes["http"].empty?
    autostop = tank.stopped?(id)
    if autostop[0]
      puts "Сработал автостоп: #{autostop[1]}"
    end
  end

rescue SystemExit, Interrupt => e
  puts e.to_s
  puts "tank stoped"
  tank.stop(id)
end
