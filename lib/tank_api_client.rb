require 'net/http'
require 'json'

# @author Ivan Stroev

class TankApiClient

  class TankError < RuntimeError
  end

  attr_accessor :host, :port

  DEFAULT_SERVER_PORT = 8888

  def initialize(host, port = DEFAULT_SERVER_PORT)
    @host = host
    @port = port
  end

  def get_uri
    URI("http://#{@host}:#{@port}")
  end

  def get_response uri
    Net::HTTP.get_response(uri).body
  end

  def get_response_json uri
    JSON.parse(get_response(uri))
  end

  # Возвращает статусы тестов в json, где ключи id сессии и значения - статусы.
  # @return [Hash] статусы тестов
  # @example
  # status(localhost)
  def status
    uri = get_uri
    uri.path = '/status'
    get_response_json(uri)
  end

  # Создает новую сессию с уникальным id и запускает танк
  # Останавливает танк на этапе start
  # @param ini [String] load.ini файл
  # @return [String]
  def run ini
    uri = get_uri
    uri.path = '/run'
    uri.query = 'break=start'

    req = Net::HTTP::Post.new(uri)
    req.body = ini
    req.content_type = 'multipart/form-data'

    res = Net::HTTP.start(uri.host, uri.port) do |http|
      http.request(req)
    end

    case res
    when Net::HTTPSuccess
      response = JSON.parse(res.body)
      response["session"]
    else
      raise JSON.parse(res.body)
    end
  end

  # Запускает танк
  # @param id [String] id сессии
  # @return [String]
  def start id
    uri = get_uri
    uri.path = '/run'
    uri.query = 'break=finished&session='+ id
    get_response(uri)
  end

  # Возвращает статус сессии
  # @note статусы running, failed, success
  # @param id [String] id сессии
  # @return [Hash]
  def get_status id
    uri = get_uri
    uri.path = '/status'
    uri.query = 'session=' + id
    get_response_json(uri)
  end

  # Возвращает этап теста
  # @note статусы running, failed, success
  # @param id [String] id сессии
  # @return [String]
  def get_stage id
    uri = get_uri
    uri.path = '/status'
    uri.query = 'session=' + id
    get_response_json(uri)["current_stage"]
  end

  # Завершает работу танка
  # @param id [String] id теста
  # @return [String]
  # @see start
  def stop id
    uri = get_uri
    uri.path = '/stop'
    uri.query = 'session=' + id
    get_response(uri)
  end

  # Возвращает массив с названиями файлов
  # @param id [String] id теста
  # @return [Array<String>]
  def get_artifacts id
    uri = get_uri
    uri.path = '/artifact'
    uri.query = 'session=' + id
    get_response_json(uri)
  end

  # Получить файл
  # @note список файлов answ_*.log, phantom_*.log, status.json, load.ini, phantom_*.conf, tank_brief.log, lunapark_*.lock, phantom_*.log, tank.log, monitoring_default__*.xml, phantom_stdout_stderr_*.log, monitoring_*.data, phout_*.log
  # @param id [String] id теста
  # @param file [String] название файла
  # @return [String]
  def get_artifact(id, file)
    file_name = get_artifacts(id).select{ |i| i[/#{file}/] }
    unless file_name[0].nil?
      uri = get_uri
      uri.path = '/artifact'
      uri.query = 'session=' + id + '&filename=' + file_name[0]
      get_response(uri)
    else
      raise(TankError, "File with name #{file} doesn't exist") # tank error
    end
  end

  # Проверяет сработал ли автостоп
  # @param id [String] id теста
  # @return [FalseClass/TrueClass, String]
  def stopped? id
    begin
      [ true, get_artifact(id, "autostop") ]
    rescue Exception => e
      [ false, e.to_s ]
    end
  end

  # Возвращает текущий RPS
  # @param id [String] id теста
  # @return [Fixnum]
  def get_current_rps id
    if get_status(id)["status"] == "running"
      stat = get_artifact(id, 'phantom_stat')
      regexp = /\d{4}\-\d{1,2}\-\d{1,2}\ [0-9]*\:[0-9]*\:[0-9]*\.[0-9]*\ \+[0-9]*\"\ \:\ /
      timestamps = stat.scan(regexp)
      if timestamps.size > 1
        stat_js = JSON.parse stat[/#{Regexp.quote(timestamps[-2])}(.*?)\,\n\"#{Regexp.quote(timestamps[-1])}/m, 1]
        stat_js["benchmark_io"]["stream_method"]["mcount"]["mcount"]["Success"]
      end
    else
      raise(TankError, "Test not run or finished") # tank error
    end
  end

  # Возвращает средний RPS
  # @param id [String] id теста
  # @return [Fixnum]
  def get_average_rps id
    stat = get_artifact(id, 'phantom_stat')
    date = /\d{4}\-\d{1,2}\-\d{1,2}\ [0-9]*\:[0-9]*\:[0-9]*\.[0-9]*\ \+[0-9]*\"\ \:\ /
    rps_stat = []
    stat.scan(date).each_cons(2) do |timestamp|
      rps_stat << JSON.parse(stat[/#{Regexp.quote(timestamp[0])}(.*?)\,\n\"#{Regexp.quote(timestamp[1])}/m, 1])["benchmark_io"]["stream_method"]["mcount"]["mcount"]["Success"]
    end
    avg_rps = rps_stat.compact.inject(0.0) { |sum, el| sum + el } / rps_stat.size
    avg_rps.to_i
  end

  # Возвращает максимальный RPS
  # @param id [String] id теста
  # @return [Fixnum]
  def get_max_rps id
    # получаем все rps-ы
    stat = get_artifact(id, 'phantom_stat')
    date = /\d{4}\-\d{1,2}\-\d{1,2}\ [0-9]*\:[0-9]*\:[0-9]*\.[0-9]*\ \+[0-9]*\"\ \:\ /
    rps_stat = []
    stat.scan(date).each_cons(2) do |timestamp|
      rps_stat << JSON.parse(stat[/#{Regexp.quote(timestamp[0])}(.*?)\,\n\"#{Regexp.quote(timestamp[1])}/m, 1])["benchmark_io"]["stream_method"]["mcount"]["mcount"]["Success"]
    end
    rps_sort = rps_stat.compact.sort do |a,b|
      b <=> a
    end
    # получаем максимальный
    config = get_artifact(id, 'lunapark_')
    rps_schedule = config.split("\n").select{|i| i[ /rps_schedule/ ]}[0].split("=")
    schedule_max_rps = rps_schedule[1].split(/\W+/).select{ |i| i[/^\d+$/] }.map(&:to_i).max
    rps_sort.reject! {|i| i > schedule_max_rps.to_i}
    # подсчет
    max_rps = rps_sort[0, 20].compact.inject(0.0) { |sum, el| sum + el } / rps_sort[0, 20].size
    max_rps.to_i
  end

  # Возвращает http и net ошибки
  # @note phout file columns: 0-time, 1-tag, 2-interval_real, 3-connect_time, 4-send_time, 5-latency, 6-receive_time, 7-interval_event, 8-size_out, 9-size_in, 10-net_code, 11-http_code
  # @param id [String] id теста
  # @return [Hash]
  #OPTIMIZE
  def get_request_codes id
    phout = get_artifact(id, 'phout')

    http_errors = Hash.new(0)
    net_errors = Hash.new(0)

    phout.lines.each do |line|
      if line.split(" ")[10] != '0'
        net_errors[line.split(" ")[10]] +=1
      else
        if line.split(" ")[11] != '200'
          http_errors[line.split(" ")[11]] +=1
        end
      end
    end
    {'http' => http_errors, 'net' => net_errors}
  end

  # Возвращает текущее время ответа
  # @param id [String] id теста
  # @return [String]
  def get_current_time id
    if get_status(id)["status"] == "running"
      stat = get_artifact(id, 'phout').split("\n")
      if stat.size > 1
        stat[-1].split(" ")[4]
      end
    else
      raise(TankError, "Test not run or finished") # tank error
    end
  end

  # Возвращаем среднее время ответа
  # @param id [String] id теста
  # @return [Fixnum]
  def get_average_time id
    stat = get_artifact(id, 'phout').split("\n")
    times = []
    stat.each do |line|
      times << line.split(" ")[4].to_i
    end
    avg_time = times.compact.inject(0.0) { |sum, el| sum + el } / times.size
    avg_time.to_i
  end

  # Возвращаем среднее время при максимальном rps
  # @param id [String] id теста
  # @return [Fixnum]
  def get_avg_time_max id
    stat = get_artifact(id, 'phout').split("\n")
    times = []
    stat.each do |line|
      times << line.split(" ")[4].to_i
    end
    times.reverse!
    avg_time = times[0, 20].compact.inject(0.0) { |sum, el| sum + el } / times[0, 20].size
    avg_time.to_i
  end
  private :get_uri, :get_response, :get_response_json

end
