Yandex-tank-api-client
======================

[yandex-tank-api-server documentation](https://github.com/yandex-load/yandex-tank-api/blob/master/README.md)

# Usage example
```ruby
tank = TankApiClient.new(:host, :port)
load_ini = IniFile.new
load_ini['phantom'] = {
  address: '127.0.0.1',
  port: '8000',
  rps_schedule: 'const(1, 20s)',
  instances: '20',
  uris: '/'}
session_id = tank.run load_ini.to_s
tank.start session_id
tank.status session_id
```
