Gem::Specification.new do |g|
  g.name = "tank_api_client"
  g.version = "0.0.1"
  g.summary = "client for the yandex-tank-api-server written in ruby"
  g.files = ["lib/tank_api_client.rb"]
  g.author = "Ivan Stroev"
end
